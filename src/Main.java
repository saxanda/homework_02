import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static int[] target() {
        int n_row = (int) (Math.random() * (5 - 1)) + 1;
        int n_column = (int) (Math.random() * (5 - 1)) + 1;
        return new int[] {n_row, n_column};
    }

    public static int[][] pyramideMatrix(int cells, int mh[][]) {

        int dim = cells +1;

        int[][] arr = new int[dim][dim];
        int ii ;
        ii = -1;
        int ij;
        ij = -1;
        //прамальовуємо таблицю
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i < (dim-cells)) {
                    ii = ii+1;
                } else {
                    ii = 0;
                }
                if (j==0) {
                    ij = ij+1;
                } else {
                    ij = ij;
                }
                arr[i][j] = Math.max(ii, ij);
                if ((i != 0) & (j != 0)){
                    arr[i][j] = 0;
                }
                else {arr[i][j] = Math.max(ii, ij);}
            }
        }
        for (int i = 0; i < mh.length; i++) {

            for (int j = 0; j < mh.length; j++) {
               if (mh[i][j] == 1){
                   arr[i+1][j+1] = 1;
               }
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        // генеруєм ціль
        int result[] = target();
        System.out.println(result[0] + "," + result[1]);

        int hit_i = 0;
        int hit_j = 0;
        int matrix_hit[][] = new int[5][5];
        String[][] matrix_symbol = new String[6][6];
        //System.out.println(matrix_hit);
        System.out.println("Let the game begin!");
        Scanner scanner = new Scanner(System.in);
        while (hit_i != result[0] && hit_j != result[1]) {
            System.out.println("Enter number of row [1-5]: ");
            hit_i = Integer.valueOf(scanner.nextLine());
            System.out.println("Enter number of column [1-5]: ");
            hit_j = Integer.valueOf(scanner.nextLine());
            matrix_hit[hit_i - 1][hit_j - 1] = 1;

            int[][] matrix = pyramideMatrix(5, matrix_hit);
            //char.class.toString()
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    //matrix_symbol[i][j] = Integer.toString(matrix[i][j]);
                    if ((matrix[i][j] == 0) && (i > 0) && (j > 0)) {
                        matrix_symbol[i][j] = "-";
                    } else if ((i == result[0]) && (j == result[1])) {
                        matrix_symbol[i][j] = "X";
                    } else if (((matrix[i][j] == 1) && (i > 0) && (j > 0))) {
                        matrix_symbol[i][j] = "*";
                    } else {
                        matrix_symbol[i][j] = Integer.toString(matrix[i][j]);
                    }
                }
            }
            StringBuilder sb = new StringBuilder();
            //Arrays.toString(matrix).replace("0", "-");
            for (String[] arr : matrix_symbol) {
                sb.append(Arrays.toString(arr)).append('\n');
            }
            System.out.println(sb);
        }

        System.out.println("You have won!");

        scanner.close();
        }
    }


